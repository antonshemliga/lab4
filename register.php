<?php
namespace LoginApp\Controller;
use LoginApp\View as View;
use LoginApp\Model as Model;
require_once("views/register-login-view.php");
require_once('./components/stringdependencies.php');

$this->registerView->renderRegisterPage($this->registerView->registerFeedback);
$this->registerView->register();
                if ($this->registerView->proceedToLogin()) {
                    $this->view->redirectPage();
                }
                else {
                    $this->registerView->redirectRegisterPage();
                }