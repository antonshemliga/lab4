<?php

namespace LoginApp\View;

class HeadHtml {
    private $title;

    public function __construct($title) {
        $this->title = $title;
    }

    public function getHtml() {

        return '<head>
            <meta charset="utf-8" />
            <title>' . $this->title . '</title>
            <link rel="stylesheet" type="text/css" href="styles/styles.css" />
            <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" />
            <style>
                body {
                    margin: 20px auto;
                }
            </style>
        </head>';
    }
}
