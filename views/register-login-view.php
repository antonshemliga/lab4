<?php 
namespace LoginApp\View;

use LoginApp\Model\LoginModel;
require_once('./components/stringdependencies.php');
require_once('register-helper.php');

class RegisterLoginView extends LoginView {
    /*moved from model*/
    private $registerUser;
    private $registerPassword;
    private $registerRepeatedPassword;
    private $registerSubmit;
    private $registerHelper;
    public $registerFeedback;

    private $time;
    private $stringdep;
    public function __construct (LoginModel $model) {
        $this->headHtml = new HeadHtml('1DV408 - Register user');
        $this->footerHtml = new FooterHtml();
        $this->stringdep = new \stringdependencies\StringDependencies();
        parent::__construct($model);

        /*moved from model*/
        // register form vars
        $this->registerHelper = new RegisterHelper();
        $this->registerUser = $this->registerHelper->getRegisterUsername();
        $this->registerPassword = $this->registerHelper->getRegisterPassword();
        $this->registerRepeatedPassword = $this->registerHelper->getRepeatedPassword();
        $this->registerSubmit = $this->registerHelper->getSubmitButton();
    }

	public function renderRegisterPage($feedback = "") {
    $render = '<html>'
    . $this->headHtml->getHtml() .
    '<body style="margin: 20px;">
    <h1>Laborationskod aa223ap</h1>
    <p><a href="index.php">Tillbaka</a></p>
    <h2>Registrera användare</h2>
		<form method="post">
      <fieldset>
      	<legend>Registrera ny användare - Skriv in användarnamn och lösenord</legend>
        <section>'
        . $feedback .
        '<label for="registerName">Användarnamn:</label><br>
        <input type="text" id="registerName" name="'.$this->stringdep->registerUserName().'" value="' . strip_tags($this->model->getUserToRegister()) . '" /><br><br>
        <label for="registerPassword">Lösenord:</label><br>
        <input type="password" id="registerPassword" name="'.$this->stringdep->registerPassword().'" /><br><br>
        <label for="registerPasswordRepeatPassword">Repetera lösenord:</label><br>
        <input type="password" id="registerPasswordRepeatPassword" name="'.$this->stringdep->registerRepeatedPassword().'" /><br><br>
        <input type="submit" name="'.$this->stringdep->registerUserAndPassword().'" value="Registrera användare" /><br><br><br>
      </fieldset>
    </form>'
    . $this->footerHtml->getHtml() .
    '</body>
    </html>';
    echo $render;
    }
    // echoes feedback from registration
    public function echoFeedback ($feedback = "") {
    	echo $feedback;
    	return;
    }

    public function redirectRegisterPage() {
        header('location: ' . $_GET["register"]);
    }

    /*********************************** 
    * registration functionality goes here *
    ***********************************/
    public function proceedToLogin () {
        if (isset($_SESSION[$this->stringdep->registerAbort()]) && $_SESSION[$this->stringdep->registerAbort()] === "proceed") {
            $_SESSION[$this->stringdep->registerAbort()] = "";
            return true;
        }
        else {
            return false;
        }
    }
    

    // if userExists is passed in as true, 
    // the other params feedback are ignored
    private function updateRegisterFeedback($userExists = false) {
        $this->registerFeedback = $this->registerHelper->displayAppropriateFeedbackToUser(
                $this->registerUser,
                $this->registerPassword,
                $this->registerRepeatedPassword,
                $userExists
       );
    }
    public function register() {
        if ($this->registerSubmit) {
            // if there are users of same name in db, 
            // set the username to input,
            // display output that username is taken
            if (in_array(strtolower($this->registerUser), $this->model->dao->existingUsersInDB()) && $this->registerUser !== ""  && $this->registerUser !== null) {
                $this->registerHelper->setUserToRegister();
                $this->updateRegisterFeedback(true);
                $_SESSION[$this->stringdep->registerFeedback()] = $this->registerFeedback; 
                // $_SESSION[$this->stringdep->registerFeedback()] = '<br><br>' . $this->registerFeedback; 
            }
            // otherwise, set feedback to inform user of registration status
            else {                
                $this->registerHelper->setUserToRegister();
                // processes the info from the post
                // sets the feedback to correspond to user input
                $this->updateRegisterFeedback();
                $_SESSION[$this->stringdep->registerFeedback()] = $this->registerFeedback; 
                // $_SESSION[$this->stringdep->registerFeedback()] = '<br><br>' . $this->registerFeedback; 
                // controls all data before sending it into db
                $this->model->dao->controlRegistration(
                    $this->registerUser,
                    ($this->model->encryptPassword($this->registerPassword)),
                    ($this->model->encryptPassword($this->registerRepeatedPassword))
                );
                }
        }
    } 
}