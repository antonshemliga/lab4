<?php

namespace LoginApp\View;
require_once('./components/stringdependencies.php');
class RegisterHelper {
	private $registerSubmit;
	private $registerUserName;
	private $registerPassword;
	private $registerRepeatedPassword;
	private $registerFeedback;
	private $sessionvars = array( 
		"registeredUser" => "", 
	); 
	private $stringdep;
	
	public function __construct () {
		$this->stringdep = new \stringdependencies\StringDependencies();
	}
	
	public function getSubmitButton () {
		return (isset($_POST[$this->stringdep->registerUserAndPassword()]) ? $_POST[$this->stringdep->registerUserAndPassword()] : "");
	}
		
	public function getRegisterUsername () {
		return (isset($_POST[$this->stringdep->registerUserName()]) ? $_POST[$this->stringdep->registerUserName()] : "");
	}

	public function getRegisterPassword () {
		return (isset($_POST[$this->stringdep->registerPassword()]) ? $_POST[$this->stringdep->registerPassword()] : "");
	}
	public function getRepeatedPassword () {
		return (isset($_POST[$this->stringdep->registerRepeatedPassword()]) ? $_POST[$this->stringdep->registerRepeatedPassword()] : "");
	}

	public function getSessionAbort () {
		return (isset($_SESSION[$this->stringdep->registerAbort()]) ? $_SESSION[$this->stringdep->registerAbort()] : "");
	}

	public function setSessionAbort ($status) {
		$_SESSION[$this->stringdep->registerAbort()] = $status;
	}

	public function setUserToRegister() {
		$_SESSION[$this->stringdep->registerUserName()] = $this->getRegisterUsername();
	}

	// check credentials for registration
	public function displayAppropriateFeedbackToUser ($username, $password, $repeatedPassword, $userExists) {
		$this->setSessionAbort("abort");
		if (strlen($username) !== strlen(strip_tags($username))) {   
			return "<p><strong>Användarnamnet innehåller ogiltiga tecken.</strong></p>";       
		}

		if ($userExists) {
			return "<p><strong>Användarnamnet är redan upptaget.</strong></p>";
		}

		// make sure username is at least 3 letters
		if (strlen($username) < 4) {
			$this->registerFeedback .= "<p><strong>Användarnamnet har för få tecken. Minst 3 tecken.</strong></p>";
		}  

		// make sure both passwords are at least 6 letters
		if (strlen($password) < 6 || strlen($repeatedPassword) < 6) {
			$this->registerFeedback .= "<p><strong>Lösenorden har för få tecken. Minst 6 tecken.</strong></p>";
		}
		
		// make sure username is at least 4 letters, both passwords are at least 6 letters
		// if passwords match, registration is successful,
		// otherwise not
		if (strlen($username) > 3 && strlen($password) > 5 && strlen($repeatedPassword) > 5) {
			if ($password === $repeatedPassword) {
				$this->setSessionAbort("proceed");
				$this->registerFeedback = "<p><strong>Registrering av ny användare lyckades</strong></p>";
			}
			else {
				return "<p><strong>Lösenorden matchar inte.</strong></p>";	
			}
		}
		return $this->registerFeedback;
	}
}