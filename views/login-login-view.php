<?php

namespace LoginApp\View;

use LoginApp\Model\LoginModel;

class LoginLoginView extends LoginView {
    private $postUsernameKey;
    private $postPasswordKey;
    private $postAutoLoginCheckedKey;
    private $postLoginButtonNameKey;
    private $stringdep;

    public function __construct(LoginModel $model, \stringdependencies\StringDependencies $stringdependency) {
        $this->stringdep = $stringdependency;
        //  sets to LoginLoginView::variable
        $this->postUsernameKey = get_class() . '::Username';
        $this->postPasswordKey = get_class() . '::Password';
        $this->postAutoLoginCheckedKey = get_class() . '::AutoLoginChecked';
        $this->postLoginButtonNameKey = get_class() . '::LoginButtonName';
        // received from user posting the login form
        $this->username = (isset($_POST[$this->postUsernameKey]) ? $_POST[$this->postUsernameKey] : "");
        $this->password = (isset($_POST[$this->postPasswordKey]) ? $_POST[$this->postPasswordKey] : "");
        $this->autoLogin = (isset($_POST[$this->postAutoLoginCheckedKey]) ? true : false);
        // LoginView constructs the model, which is shared with the controller
        parent::__construct($model);
        $this->headHtml = new HeadHtml('1DV408 - Login');
    }
    
    // if user presses login
    public function wasLoginButtonClicked() {
        return isset($_POST[$this->postLoginButtonNameKey]);
    }

    // if user clicks register
    public function doesUserWantToRegister () {
        return isset($_GET['register']) ? true : false;
    }
    
    // username from user's input
    public function getUsername() {
        return $this->username;
    }
    
    // password from user's input
    public function getPassword() {
        return $this->password;
    }
    
    // if user wants to save credentials and autologin
    public function wasAutoLoginChecked() {
        return $this->autoLogin;
    }

    // check for stored cookies
    public function doesLoginCookieExist() {
        return ((isset($_COOKIE[$this->cookieUsernameKey]) || (isset($_COOKIE[$this->cookieEncryptedPasswordKey]))));
    }

    // render loginpage
    public function renderPage() {
        if (isset($_COOKIE[$this->cookieEncryptedPasswordKey])) {
            // TODO: something
        }
        // <form action="' . $_SERVER['PHP_SELF'] . '" method="post">
        echo '<html>'
        . $this->headHtml->getHtml() .
        '<body style="margin: 20px;">
            <h1>Laborationskod aa223ap</h1>
            <p><a href="?register">Registrera ny användare</a></p>
            <h2>Ej Inloggad</h2>

            <form action="hannes" method="post">
                <fieldset>
                    <legend>Login - Skriv in användarnamn och lösenord</legend>' .
                ($this->model->getNotification() ? '<p>' . $this->model->getNotification() . '</p>' : '')
                . ' <label for="usernameId">Användarnamn:</label>
                    <input type="text" name="' . $this->postUsernameKey . '" id="usernameId" value="' . strip_tags($this->model->getUserToRegister()) . '" autofocus />
                    <label for="passwordId">Lösenord:</label>
                    <input type="password" name="' . $this->postPasswordKey . '" id="passwordId" />
                    <label for="autoLoginId">Håll mig inloggad:</label>
                    <input type="checkbox" name="' . $this->postAutoLoginCheckedKey . '" id="autoLoginId"' .
                ($this->autoLogin ? "checked" : "") . ' />
                    <input type="submit" name="' . $this->postLoginButtonNameKey . '" value="Logga in" />
                </fieldset>
            </form>
            <p></p>' .
            $this->footerHtml->getHtml() .
        '</body>
        </html>
        ';
    }

    // set cookies if box is checked by user
    public function setCookiesIfAutoLogin() {
        if ($this->autoLogin) {
            $encryptedPassword = $this->model->encryptPassword($_POST[$this->postPasswordKey]);
            setcookie($this->cookieUsernameKey, $_POST[$this->postUsernameKey], time()+2592000, '/'); //expire in 30 days
            setcookie($this->cookieEncryptedPasswordKey, $encryptedPassword, time()+2592000, '/');
        }
    }
}