<?php 

namespace db;

require_once('./views/register-helper.php');

class DataAccessObject {
	private $username;
	private $password;
	private $repeatedPassword;
	private $registerHelper;
	private $allUsers = array();
	private $pdo;
	
	public function __construct() {
		$this->registerHelper = new \LoginApp\View\RegisterHelper();
		$this->username = $this->registerHelper->getRegisterUsername();
		$this->password = $this->registerHelper->getRegisterPassword();
		$this->repeatedPassword = $this->registerHelper->getRepeatedPassword();
		// ersmajestat.com.mysql
		// 
		$this->pdo = new \PDO('mysql:host=ersmajestat.com.mysql;dbname=ersmajestat_com', 'ersmajestat_com', 'aa223ap');
		// $this->pdo = new \PDO('mysql:host=localhost;dbname=dev', 'hejadmin', 'frassebus');
		// $this->pdo = new \PDO('mysql:unix_socket=/var/run/mysqld/mysqld.sock;dbname=dev', 'hejadmin', 'frassebus');
				//var/run/mysqld/mysqld.sock
		$this->selectAllRegisteredUsers();
	}

	private function selectAllRegisteredUsers () {
		$sql = 'SELECT name, password FROM users';
		$this->allUsers = $this->pdo->query($sql)->fetchAll();
		return $this->allUsers;
	}
	private function addUser ($username, $password) {
		try {
			$sql = "INSERT INTO users (name, password) VALUES (?, ?)";
			$query = $this->pdo->prepare($sql);
			$query->bindParam(1, $username);
			$query->bindParam(2, $password);
			$query->execute();
		}
		catch (Error $e) {
			echo "oops";
			throw new Error($e);
		}
	}
	private function illegalCharactersExist () {
		// echo "illegalCharactersExist";
	}

	// makes sure username isn't already in db
	private function usernameIsUnique ($username) {
		return !in_array(strtolower($username), $this->existingUsersInDB());
	}
	
	// makes sure passwords match
	private function passwordsMatch ($password, $repeatedPassword) {
		return $password === $repeatedPassword;
	}

	// makes sure passwords match
	private function passwordsExist ($password, $repeatedPassword) {
		return !($password === null || $repeatedPassword === null);
	}

	/*
	* this is a top level function used for controlling input:
	*	> makes sure there are no illegal characters
	*	> makes sure username is unique in db
	*	> makes sure all fields are filled in correctly
	*	> returns true if all passes, and user can be inserted into db
	*/
	//  $this->passwordsExist($password, $repeatedPassword)
	public function controlRegistration($username, $password, $repeatedPassword) {
		if ($this->registerHelper->getSessionAbort() === "proceed" && $this->passwordsMatch($password, $repeatedPassword) && $this->usernameIsUnique($username) && $this->passwordsExist($password, $repeatedPassword))  {
				$this->addUser($username, $password);
		}
		// abort otherwise
		// return silently to your fav reg screen
		else {	
			return;
		}
	}

	// returns array of all existing users in lowercase
	public function existingUsersInDB () {
		$names = array();
		for ($i = 0; $i < count($this->allUsers); $i++) {
  		$names[$i] = strtolower($this->allUsers[$i]["name"]);
		}
		return $names;
	}

	public function getLoginCredentials () {
		$loginCredentials = array();
		for ($i = 0; $i < count($this->allUsers); $i++) {
			$loginCredentials[($this->allUsers[$i]["name"])] = $this->allUsers[$i]["password"];
		}
		return $loginCredentials;
	}

}