<?php
	namespace stringdependencies;

	class StringDependencies {
		public function __construct () {
			// echo "stringdep";
		}
		public function registerAbort () {
			return "LoginApp::RegisterAbort";
		}

		public function registerUserName () {
			return "LoginApp::RegisterUserName";
		}

		public function registerPassword () {
			return "LoginApp::RegisterPassword";
		}

		public function registerRepeatedPassword () {
			return "LoginApp::RegisterRepeatPassword";
		}

		public function registerFeedback () {
			return "LoginApp::RegisterFeedback";
		}

		public function registerUserAndPassword () {
			return "LoginApp::RegisterUserAndPassword";	
		}
	}