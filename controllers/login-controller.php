<?php
/***************************************
verbose comments to follow handover code
****************************************/
namespace LoginApp\Controller;
use LoginApp\View as View;
use LoginApp\Model as Model;

require_once("models/login-model.php");
require_once("views/login-view.php");
require_once("views/login-login-view.php");
require_once("views/register-login-view.php");
require_once("views/logged-in-login-view.php");
require_once('./components/stringdependencies.php');

class LoginController {
    private $view;
    private $model;
    private $registerView;
    private $stringdep;
    /*
    * sets the model to use
    */
    public function __construct() {
        $this->stringdep = new \stringdependencies\StringDependencies();
        $this->model = new Model\LoginModel($this->stringdep);
        $this->view = new View\LoginLoginView($this->model, $this->stringdep);
        $this->registerView = new View\RegisterLoginView($this->model);
    }

    /*
        runs when index.php is init
     */
    public function start() {
        /*
            check for previous log in
         */
        if ($this->model->isLoggedIn()) {
            // render back end view
            $this->view = new View\LoggedInLoginView($this->model);
            // if cookies has been tempered with, 
            // don't regenerate id, 
            // logout user,
            // don't show feedback,
            // redirect
            if (!$this->model->isSessionIntegrityOk()) {
                session_regenerate_id(false);
                $this->model->logout();
                $this->model->unsetNotification(); //don't show a message to session hijacker that the hijacked session was logged out.
                $this->view->redirectPage();
            // if user has previously logged in, but chose to log out:
            // logout user, 
            // clear all set cookies, 
            // redirect    
            } elseif ($this->view->wasLogoutLinkClicked()) {
                $this->model->logout();
                $this->view->clearCookies();
                $this->view->redirectPage();
            // otherwise, user is logged out
            // render log in page,
            // remove feedback
            } else {
                $this->view->renderPage();
                $this->model->unsetNotification();
            }
        } else {
            // user has to provide auth credentials
            // redirects on successful auth
            $willRedirect = false;
            // if register was clicked 
            if ($this->view->doesUserWantToRegister()) {
                $this->registerView->renderRegisterPage($this->registerView->registerFeedback);
                $this->registerView->register();
                if ($this->registerView->proceedToLogin()) {
                    $this->view->redirectPage();
                }
                else {
                    $this->registerView->redirectRegisterPage();
                }
            }
            else {
                // if login was pressed, 
                // check auth credentials
                if ($this->view->wasLoginButtonClicked()) {
                    // logs in with user and pass,
                    // passes in boolean deciding if autologin is checked,
                    // if autologin, sets cookies with expiration
                    if ($this->model->tryLogin($this->view->getUsername(), $this->view->getPassword(), $this->view->wasAutoLoginChecked())) {
                        $this->view->setCookiesIfAutoLogin();
                        $this->model->setServersideCookieExpirationIfAutoLogin();
                    }
                    // redirects to backend
                    $willRedirect = true;
                // if stored cookies exist in browser    
                } elseif ($this->view->doesLoginCookieExist()) {
                    // user auths through cookies, redirected to backend
                    if ($this->model->tryLoginWithCookie($this->view->getUsernameFromCookie(), $this->view->getEncryptedPasswordFromCookie())) {
                        $willRedirect = true;
                    // auth fails, clear cookies    
                    } else {
                        $this->view->clearCookies();
                    }
                }
                // displays backend if auth has been successful,
                // resets login otherwise
                if ($willRedirect) {
                    $this->view->redirectPage();
                } else {
                    $this->view->renderPage();
                    $this->model->unsetNotification();
                }
            }
        }
    }
}